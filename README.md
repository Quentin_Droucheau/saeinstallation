# Installation SAE

Cette installation consiste à mettre en place un environnement de travail afin de pouvoir réaliser nos travaux dans les différents cours.
Toutes les commandes seront à effectuer dans le terminal.

## 1 - Installation de xubuntu (dual boot)

### 1.1 - Téléchargement du système xubuntu

Prérequis: aucun

Pour installer xubuntu sur votre machine, vous devez installer l'iso sur le site de [xubuntu](https://xubuntu.fr/), il permettra de créer une clé bootable afin d'installer xubuntu sur votre ordinateur.

![Xubuntu](img/Xubuntu.png "Xubuntu")

### 1.2 - Télécharger rufus

Prérequis: aucun

Afin de télécharger xubuntu en dual boot sur un ordinateur, nous devons créé une clé bootable qui servira à démarrer le système pour pouvoir l'installer, pour cela, Rufus permet de créer la clé bootable.
[télécharger rufus ici](https://rufus.ie/fr/#)

### 1.3 - Crée une clé bootable

Prérequis: 1.1 et 1.2

Pour créer une clé bootable, lancer le logiciel Rufus qui ressemble à ceci:

![Rufus](img/Rufus.png "Rufus")

Puis:
- Sélectionnez le périphérique sur lequel vous souhaitez faire la clé bootable
- Sélectionnez l'iso que vous avez téléchargé précédemment
- Vous pouvez laisser les autres paramètres par défaut
- Appuyez ensuite sur démarrer.

Une fois l'installation terminée vous pouvez fermer l'application en appuyant sur fermer.

### 1.4 - Démarrez xubuntu sur votre ordinateur

Prérequis: 1.3

Pour démarrer xubuntu sur votre ordinateur, plusieurs méthodes sont possibles: 

#### Méthode 1:
Eteignez votre ordinateur puis allumé le en restant appuyer sur la touche F12, cela permet d'ouvrir le boot menu. Sur certain ordinateur, le boot menu peut etre par défaut désactivé, vous pouvez l'activer dans le bios pour cela: 
- Allumez votre ordinateur en appuyant sur la touche F2
- Allez dans le menu boot
- Changez la valeur de "F12 boot Menu" en enabled
- Appuyez sur F10 pour sauvegarder et enregistrer

Une fois dans le boot menu, vous pouvez sélectionner votre clé où est installer le système d'exploitation.

![BootMenu](img/BootMenu.png "BootMenu")

Les touches peuvent changer en fonction de l'ordinateur, vous trouverez les touches en fonction de la marque de l'ordinateur [ici](https://lecrabeinfo.net/liste-des-touches-pour-acceder-au-bios-uefi-acer-asus-dell-lenovo-hp.html)

#### Méthode 2:
- Allez dans les paramétres de votre ordinateur
- Mise a jour et sécurité 
- Récupération > démarrage avancé > Utilisé un périphérique
- Séléctionnez Usb drive

Une fois le système lancé depuis la clé sélectionnez "try or install xubuntu" puis entrer
![Try](img/Try.png "Try")

### 1.5 - Configurez xubuntu 

Prérequis: 1.4

- Sélectionnez votre langue puis appuyez sur install Xubuntu
- Choissisez la disposition du clavier puis continuer
- Sélectionnez l'installation normale et sélectionnez télécharger les mises a jour pendant l'installation si vous le souhaitez 
- IMPORTANT: séléctionnez installer xubuntu à coté de Windows si vous souhaitez garder Windows, autrement vous pouvez effacer le disque et installer Xubuntu (cela efface toute vos données et Windows). Puis appuyer sur "Installer Maintenant"
- Entrez votre nom et séléctionnez un mot de passe puis continuer.

Une fois l'installation terminé un message vous indiquera d'enlever la clé et d'appuyer sur entrer:
![Remove](img/Remove.png "Remove")

## 2 - Installation de visual studio code sur Xubuntu

Visual studio code est une application gratuite et légère qui vous permettra d'effectuer et d'exécuter votre code facilement grace à de nombreuses extensions disponibles

```
    sudo snap install code --classic
```
Pour vérifier que visual studio code est bien installé:
```
    code --version
```

## 3 - Installation de java

Pour pouvoir exécuter votre code java, vous devez d'abord le compiler, pour cela vous devez installer Javac: 

```
    sudo apt install default-jdk
```

Ensuite vous pouvez installer java pour exécuter votre code compiler:

```
    sudo apt install default-jre
```

Vous pouvez ensuite vérifier que java et javac sont bien installés:
```
    java --version
    javac --version
```

### 4 - Installation de git 

Installer git vous permettra d'utiliser github et gitlab, pour cela effectuer la commande suivante:

```
    sudo apt install git
```

Pour vérifier que git est bien installé

```
    git --version
```

Configurez l'email et le nom d'utilisateur:

```
    git config --global user.email "votre adresse mail"
    git config --global user.name "Votre nom d'utilisateur"
```

### 5 - Installation de python 

Afin d'installer python executer la commande suivante:

```
    sudo apt install python3
```

Pour installer pytest vous aurez besoin d'installer le gestionnaire de paquets pip qui permet d'installer et de gérer les paquets écrit en python.

```
    sudo apt install python3-pip
```

Vous pouvez ensuite installer pytest grace au gestionnaire de paquets pip:

```
    pip install pytest
```

Pour vérifier que python et pytest sont bien installés:
```
    python3 --version
    pytest --version
```

### 6 - Installation de docker

Pour installer docker sur votre machine, executer la commande suivante:
```
    sudo apt install docker.io
```

Pour vérifier que docker est bien installé:

```
    docker --version
```

### 7 - Installation des extensions Visual studio code

#### 7.1 - Génération de docstring python
```
code --install-extension njpwerner.autodocstring
```
#### 7.2 - Exécution / debugger python
```
code --install-extension ms-python.python
```
#### 7.3 - Gitlab
```
code --install-extension GitLab.gitlab-workflow
```
#### 7.4 - Débugger / Code completion ... pour Java
```
code --install-extension vscjava.vscode-java-pack
```
#### 7.5 - HTML / CSS
```
code --install-extension ecmel.vscode-html-css
```
Vous avez désormais un environnement de travail fonctionnel.